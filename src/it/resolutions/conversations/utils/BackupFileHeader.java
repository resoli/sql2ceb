/**
 *      Copyright (c) 2022 Roberto Resoli
 *      Adapted from https://github.com/iNPUTmice/Conversations/blob/master/src/main/java/eu/siacs/conversations/utils/BackupFileHeader.java - Copyright (c) Daniel Gultsch 
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License
 *      as published by the Free Software Foundation; version 3
 *      of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package it.resolutions.conversations.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BackupFileHeader {

    private static final int VERSION = 1;

    private final String app;
    private final String jid;
    private final long timestamp;
    private final byte[] iv;
    private final byte[] salt;


    @Override
    public String toString() {
        return "BackupFileHeader{" +
                "app='" + app + '\'' +
                ", jid=" + jid +
                ", timestamp=" + timestamp +
                ", iv=" + HexHelper.bytesToHex(iv) +
                ", salt=" + HexHelper.bytesToHex(salt) +
                '}';
    }

    public BackupFileHeader(String app, String jid, long timestamp, byte[] iv, byte[] salt) {
        this.app = app;
        this.jid = jid;
        this.timestamp = timestamp;
        this.iv = iv;
        this.salt = salt;
    }

    public void write(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(VERSION);
        dataOutputStream.writeUTF(app);
        dataOutputStream.writeUTF(jid);
        dataOutputStream.writeLong(timestamp);
        dataOutputStream.write(iv);
        dataOutputStream.write(salt);
    }

    public static BackupFileHeader read(DataInputStream inputStream) throws IOException {
        final int version = inputStream.readInt();
        if (version > VERSION) {
            throw new IllegalArgumentException("Backup File version was " + version + " but app only supports up to version " + VERSION);
        }
        String app = inputStream.readUTF();
        String jid = inputStream.readUTF();
        long timestamp = inputStream.readLong();
        byte[] iv = new byte[12];
        inputStream.readFully(iv);
        byte[] salt = new byte[16];
        inputStream.readFully(salt);

        return new BackupFileHeader(app, jid, timestamp, iv, salt);

    }

    public byte[] getSalt() {
        return salt;
    }

    public byte[] getIv() {
        return iv;
    }

    public String getJid() {
        return jid;
    }

    public String getApp() {
        return app;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
