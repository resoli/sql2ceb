/**
 *      Copyright (c) 2022 Roberto Resoli
 *      Adapted from https://github.com/iNPUTmice/Conversations/blob/master/src/main/java/eu/siacs/conversations/services/ExportBackupService.java - Copyright (c) Daniel Gultsch 
 *
 *
 *      This program is free software; you can redistribute it and/or
 *      modify it under the terms of the GNU General Public License
 *      as published by the Free Software Foundation; version 3
 *      of the License.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
package it.resolutions.conversations.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Console;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.nio.charset.StandardCharsets;

import java.util.HashMap;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPOutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

public class Sql2Ceb {

    public static final String KEYTYPE = "AES";
    public static final String CIPHERMODE = "AES/GCM/NoPadding";
    public static final String PROVIDER = "BC";

    public static final String MIME_TYPE = "application/vnd.conversations.backup";

    public static byte[] getKey(final String password, final byte[] salt) throws InvalidKeySpecException {
        final SecretKeyFactory factory;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
        return factory.generateSecret(new PBEKeySpec(password.toCharArray(), salt, 1024, 128)).getEncoded();
    }



    private List<File> sql2ceb(File decfile) throws Exception {
        final List<File> files = new ArrayList<>();
	
	    System.out.println("\nReading account details from '" + decfile + "' ...\n");

	    HashMap<String, String> account = readAccountDetails(decfile);

	//String jid = backupFileHeader.getJid();
	    final String jid = account.get("username")+"@"+account.get("server");
	    final String uuid = account.get("uuid");
/*
	final Console console = System.console();

	final String password =
                new String(
                        console.readPassword(
                                "Enter password for "
                                        + jid 
                                        + ": "));
*/ 
	   
            final File cebfile = new File(System.getProperty("user.dir"), jid + ".ceb");

   	    BackupFileHeader backupFileHeader = null;
 	   	
            if(cebfile.exists()){
  	       System.out.println("Found candidate backup file for " + jid + ": " + cebfile.getAbsoluteFile() ); 
  	       System.out.println("Reading backup file header ...\n"); 
	       backupFileHeader = readBackupHeader(cebfile);
            } else { 
	       System.out.println("'" + cebfile.getAbsoluteFile() + "', the expected backup file for '" + jid + "' was not found!");  
	    }
  	        
            if(backupFileHeader == null){
               System.out.println("Creating backup file header with current timestamp and random encryption parameters ...\n"); 
	       backupFileHeader = createBackupHeader(jid);
	    }
   
	    System.out.println("\n" + backupFileHeader + "\n");

	    System.out.println(String.format("\n=== Exporting data for account %s (%s) ===\n", jid, uuid));

	    final String password = account.get("password");
            
	    //New backup generation
            final File file = new File(System.getProperty("user.dir"), "custom_" + jid + ".ceb");
            files.add(file);
            final File directory = file.getParentFile();
            if (directory != null && directory.mkdirs()) {
                System.out.println("created backup directory " + directory.getAbsolutePath());
            }
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            final DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
            backupFileHeader.write(dataOutputStream);
            dataOutputStream.flush();

   	    java.security.Security.addProvider(new BouncyCastleProvider());

            //final Cipher cipher = Compatibility.twentyEight() ? Cipher.getInstance(CIPHERMODE) : Cipher.getInstance(CIPHERMODE, PROVIDER);
            //final Cipher cipher = Cipher.getInstance(CIPHERMODE);
            final Cipher cipher = Cipher.getInstance(CIPHERMODE, PROVIDER);
            final byte[] key = getKey(password, backupFileHeader.getSalt());
            SecretKeySpec keySpec = new SecretKeySpec(key, KEYTYPE);
            IvParameterSpec ivSpec = new IvParameterSpec(backupFileHeader.getIv());
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            CipherOutputStream cipherOutputStream = new CipherOutputStream(fileOutputStream, cipher);

            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(cipherOutputStream);
	    PrintWriter writer = new PrintWriter(gzipOutputStream);

     	    final FileInputStream decInputStream = new FileInputStream(decfile);
            final BufferedReader reader = new BufferedReader(
                          new InputStreamReader(decInputStream, StandardCharsets.UTF_8));
 
            try {
         
	      int i;
              while ((i = reader.read()) != -1) 
                writer.write((char)i);

	    } catch (IOException e) {
              System.err.println("Error opening an input stream on " + decfile.getAbsoluteFile());
              System.exit(1);
            }
	    
	    reader.close();

            writer.flush();
            writer.close();

            System.out.println("written backup to " + file.getAbsoluteFile());
            
        return files;

    }


    private BackupFileHeader createBackupHeader(String jid){

       final byte[] IV = new byte[12];
       //final byte[] IV = HexHelper.hexToBytes(IVString); 
       final byte[] salt = new byte[16];
       //final byte[] salt = HexHelper.hexToBytes(saltString);
       final SecureRandom secureRandom = new SecureRandom();
       secureRandom.nextBytes(IV);
       secureRandom.nextBytes(salt);

       BackupFileHeader backupFileHeader = new BackupFileHeader("Conversations", jid, System.currentTimeMillis(), IV, salt);

       return backupFileHeader;
    }

    private BackupFileHeader readBackupHeader(File cebfile) {

        BackupFileHeader backupFileHeader = null;
        try {

           final FileInputStream cebInputStream = new FileInputStream(cebfile);
           final DataInputStream dataInputStream = new DataInputStream(cebInputStream);

            backupFileHeader = BackupFileHeader.read(dataInputStream);
        } catch (FileNotFoundException e) {
            System.err.println(cebfile.getAbsolutePath() + " not found");
            System.exit(3);
        } catch (IOException ioe) {
            System.err.println(cebfile.getAbsolutePath() + " does not seem to be a valid backup file");
            System.exit(3);
        }

	return backupFileHeader;
    }

   private HashMap<String, String> readAccountDetails(File decfile){

      final BufferedReader reader;
      HashMap<String, String> accountDetails = null;
      try {
	
              reader = new BufferedReader(new FileReader(decfile));

      	      String accountLine = reader.readLine();

	      int start = accountLine.indexOf("(uuid");

              String names = accountLine.substring(start);
	      int end = names.indexOf(")");

	      names = names.substring(1,end);

              String[] name = names.split(",");
	      for(int i = 0; i < name.length; i++)
                name[i] = name[i].trim();

	      start = accountLine.indexOf("VALUES(");
              String values = accountLine.substring(start);
	      end = values.indexOf(")");

	      values = values.substring(7,end);

	      String[] value = new String[name.length];
	      int i;
	      for(i=0; i < value.length-1 ; i++) {
                end = values.indexOf(",");
		start = values.substring(0,end).indexOf("'"); 
		values = values.substring(start+1);
		if (start != -1){
			end = values.indexOf("'");
                	value[i] = values.substring(start, end).trim();
			end = values.indexOf(",",end);
		} else {
			end = values.indexOf(",");
                	value[i] = values.substring(0,end).trim();
		}
		values = values.substring(end+1);
              }
	      //last value
              start = values.indexOf("'"); 
              end = values.indexOf("'", start+1); 
              value[i] = values.substring(start+1,(end > 0)?end:values.length());
	      
	      accountDetails = new HashMap<String, String>();
	      for(i = 0; i < name.length; i++){
	        //System.out.println(name[i] + "=" + value[i]);
		accountDetails.put(name[i], value[i]);
	      }
	      reader.close();

      } catch (IOException e) {
              System.err.println("Error opening an input stream on " + decfile.getAbsoluteFile());
              System.exit(1);
      }

      return accountDetails;
   }

   public static void main(String[] args){


	if(args.length < 1){
	       System.out.println("You have to provide the clear text sql file name!");
       	       System.exit(1);	       
	}
	
	Sql2Ceb s2c = new Sql2Ceb();


        final File decfile = new File(args[0]);
        if(!decfile.exists()){
	   System.out.println("ERROR! '" + decfile.getAbsoluteFile() + "' not found!");  
	   System.exit(2);
	}

	try{
	  
           s2c.sql2ceb(decfile);

	} catch(Exception e){
		System.out.println(e);
	} 
   }

}
