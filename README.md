# sql2ceb

A tool to recreate a [Conversations](https://github.com/iNPUTmice/Conversations) .ceb backup from the clear text sql.

It can be useful when a backup fails to restore because of a few non printable characters disrupting sql commands execution at restore time, and you want to remove that chars. (You can guess what chars running the [ceb2txt](https://github.com/iNPUTmice/ceb2txt) tool).  

When you know what to remove, you can get the clear text sql from the original backup with the `ceb2sqlgz` command from the excellent [ceb-tools](https://github.com/Yuubi-san/ceb-tools).

`ceb2sqlgz <jid>.ceb | gunzip > <jid>.dec`

Find the disturbing content, remove it and reencrypt with `sql2ceb`. 

All the code is derived from the original [Conversations ExportBackupService](https://github.com/iNPUTmice/Conversations/blob/master/src/main/java/eu/siacs/conversations/services/ExportBackupService.java) and related.

## Build

Building and running were tested under **openjdk11**.

The only external requirement is the BouncyCastle provider for the encryption algorithm implementation. You can find the library (`bcprov-*.jar`) here: https://bouncycastle.org/latest_releases.html. In this example we use `bcprov-jdk15on-1.56.jar`, put in a `lib` subdir:

```
mkdir bin
javac -cp src:lib/bcprov-jdk15on-1.56.jar -d bin  src/it/resolutions/conversations/utils/Sql2Ceb.java
```

## Run

You have to provide the clear text sql in the same directory where you run the tool, and provide its name as a parameter. All xmpp account details needed are read from it; be careful that that the file contains the password in clear, and secret keys: see [ceb-tools Readme](https://github.com/Yuubi-san/ceb-tools) under **Usage examples** and **Security & privacy considerations**. 

If the original backup, named `<jid>.ceb` is found in the same directory, the tool reads the backup file header from it. If not, the backup file header will be generated with the current timestamp (different from the original, of course), and random `salt` and `iv` encryption parameters.

```
java -cp bin:lib/bcprov-jdk15on-1.56.jar it.resolutions.conversations.utils.Sql2Ceb <clear text sql file name>
```

The generated backup has a `custom_` prefix in the name: `custom_<jid>.ceb`. If the backup file header was copied from the original backup, and no modification was made to the sql file, `custom_<jid>.ceb` and the original `<jid>.ceb` will be identical.
